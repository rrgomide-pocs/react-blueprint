import React from 'react';
import { Navbar, Alignment, Button } from '@blueprintjs/core';

export const MyNavbar = props => {
  return (
    <Navbar>
      <Navbar.Group align={Alignment.LEFT}>
        <Navbar.Heading>POC com Blueprint e React</Navbar.Heading>
        <Navbar.Divider />
        <Button className="bp3-minimal" icon="home" text="Home" />
        <Button className="bp3-minimal" icon="document" text="Files" />
      </Navbar.Group>

      <Navbar.Group align={Alignment.RIGHT}>
        <Navbar.Heading>
          Feito por: <strong>Raphael Gomide</strong>
        </Navbar.Heading>
      </Navbar.Group>
    </Navbar>
  );
};
