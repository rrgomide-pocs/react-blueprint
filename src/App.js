import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { Button, Intent, Spinner } from '@blueprintjs/core';
import { MyNavbar } from './components/MyNavbar';

// using JSX:
const mySpinner = <Spinner intent={Intent.PRIMARY} />;

class App extends Component {
  render() {
    return (
      <div className="App">
        <MyNavbar />
      </div>
    );
  }
}

export default App;
